const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    path: __dirname + '/dist',
    filename: 'index_bundle.js',
    clean: true
  },
  plugins: [
    new HtmlWebpackPlugin({
      hash: true,
      title: 'Juego de numeros',
      header: 'Juego de numeros',
      metaDesc: 'Caso Practico 3: Juego de numeros',
      template: './src/index.html',
      filename: 'index.html',
      inject: 'body'
    })
  ],
  module: {
    rules: [
      {
        test: /\.(scss)$/,
        use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
      }
    ]
},
resolve: {
  alias: {
     vue: 'vue/dist/vue.js'
  }
},
mode: 'development',
devServer: {
  static: {
    directory: path.join(__dirname, 'public'),
  },
  compress: true,
  port: 9000,
  open: true,
},
};
