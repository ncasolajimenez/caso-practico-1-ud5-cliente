import 'bootstrap';
import Vue from 'vue'
import './scss/app.scss';

let app;
app = new Vue({
  el: '#app',
  data: {
    divs: [
      {
        "id": 1,
        "color": "#00FFFF",
        "clicked": false
      },
      {
        "id": 2,
        "color": "#7FFF00",
        "clicked": false
      },
      {
        "id": 3,
        "color": "#FF8C00",
        "clicked": false
      },
      {
        "id": 4,
        "color": "#FF1493",
        "clicked": false
      },
    ],
    orden: [0],
    avisos: {
      "tipo": "alert-primary",
      "mensaje": "Haz click para jugar"
    }
  },
  created: function () {
    this.divs = this.shuffleArray(this.divs);
  },
  methods: {
    shuffleArray(array) {
      for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
      }
      return array;
    },
    clickDiv(item) {
      let ultimo_numero = this.orden[this.orden.length - 1];
      if(item.id === ultimo_numero+1) {
        this.orden.push(item.id);
        item.clicked = true;
        if(this.orden.length === 5) {
          this.avisos.mensaje = "¡Enhorabuena!";
          this.avisos.tipo = "alert-success";
        }
      } else {
        this.avisos.mensaje = "Error en la secuencia";
        this.avisos.tipo = "alert-danger";
      }
    },
    reload() {
      window.location.reload();
    }
  }
});
